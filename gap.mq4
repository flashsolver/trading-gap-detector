//+------------------------------------------------------------------+
//|                                                          gap.mq4 |
//|                                                 Karim Abdelwahab |
//|                                         http://twitter.com/incoe |
//+------------------------------------------------------------------+
#property copyright "Karim Abdelwahab"
#property link      "http://twitter.com/incoe"
#property version   "1.04"
#property strict
#property indicator_chart_window

enum ENUM_SYMBOLS
{
	SYMBOL_CURRENT,	// Check current chart symbol only
	SYMBOL_CHARTS,	// Check all opened charts
	SYMBOL_ALL,		// Check all market watch symbols
};

input ENUM_SYMBOLS			inSymb				= SYMBOL_CHARTS;		// Symbols
input double				inGapSize			= 100;					// Minimal gap size (0 - spread)
input ENUM_TIMEFRAMES		inPeriod			= PERIOD_CURRENT;		// Time frame
input bool					inAlert				= true;					// Show alerts
input bool					inPlaySound			= false;				// Play sound
input bool              inDebug           = true;          // Debug output

datetime times[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
	EventSetTimer(1);
	ArrayResize(times, 0);
	Comment("Looking for gaps");
	return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason)
{
	Comment("");
}

void prepare()
{
	int size = -1;
	if (inSymb == SYMBOL_CURRENT && ArraySize(times) != 1)
		size = ArrayResize(times, 1);
	if (inSymb == SYMBOL_ALL && ArraySize(times) != SymbolsTotal(true))
		size = ArrayResize(times, SymbolsTotal(true));
	/*if (inSymb == SYMBOL_CHARTS)
	{
		int s = 0;
		long nextchart = ChartFirst();
		do
			++s;
		while ((nextchart = ChartNext(nextchart)) != -1);
		if (ArraySize(times) != s)
			size = ArrayResize(times, 1);
	}*/
	if (size > 0)
		ArrayFill(times, 0, WHOLE_ARRAY, 0);
}

bool checkForGap(const string symb, long idx)
{
	ENUM_TIMEFRAMES tf = (idx <= 0) ? inPeriod : ChartPeriod(idx);
	tf = (inPeriod != PERIOD_CURRENT) ? inPeriod : tf;
	tf = (tf == 0) ? (ENUM_TIMEFRAMES)Period() : tf;
	
	datetime dt = iTime(symb, tf, 0);
	if (idx > 0)
	{
		ObjectCreate(idx, "TIMEPOINT", OBJ_ARROW, 0, dt - PeriodSeconds(Period()), 0);
		if (ObjectGetInteger(idx, "TIMEPOINT", OBJPROP_TIME) == dt)
			return false;
		ObjectSetInteger(idx, "TIMEPOINT", OBJPROP_TIME, dt);
	}
	else
	{
		if (dt == times[-idx])
			return false;
		times[-idx] = dt;
	}
	
	double close = iClose(symb, tf, 1);
	double open = iOpen(symb, tf, 0);
	if (close <= 0 || open <= 0)
	   return false;
	   
	int adj = (MathAbs(MarketInfo(symb, MODE_DIGITS) - 5) < Point || MathAbs(MarketInfo(symb, MODE_DIGITS) - 3) < Point) ? 10 : 1;
	if (StringFind(symb, "GOLD") >= 0 || StringFind(symb, "XAU") >= 0)
	   adj = 10;
	double spread = (inGapSize == 0) ? MarketInfo(symb, MODE_SPREAD)*MarketInfo(symb, MODE_POINT) : inGapSize*adj*MarketInfo(symb, MODE_POINT);
	
	if (MathAbs(open - close) > spread)
	{
		if (inDebug)
		   Print(StringFormat("Gap %s %s previous price %.5f current price %.5f (%.5f)",
		      symb, TimeToString(iTime(symb, tf, 0)), close, open, spread));
		return true;
	}
	
	return false;
}

void OnTimer()
{
	prepare();
	string res = "";
	if (inSymb == SYMBOL_CURRENT)
	{
		if (checkForGap(Symbol(), 0))
			res = Symbol();
	}
	else if (inSymb == SYMBOL_ALL)
	{
		for (int i = SymbolsTotal(true)-1; i >= 0; --i)
		{
			string symb = SymbolName(i, true);
			if (checkForGap(symb, -i))
				res += symb + " ";
		}
	}
	else
	{
		long nextchart = ChartFirst();
		do
		{
			string symb = ChartSymbol(nextchart);
			if (checkForGap(symb, nextchart))
				res += symb + " ";
		}
		while ((nextchart = ChartNext(nextchart)) != -1);
	}
	
	if (res == "")
		return;
		
	if (inAlert)
		Alert("Gap was detected! ", res);
	if (inPlaySound)
		PlaySound("alert.wav");
	StringReplace(res, " ", "\n");
	Comment("Gap was detected:\n", res);
}

//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
	return(rates_total);
}
//+------------------------------------------------------------------+
